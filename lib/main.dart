import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Hello")),
      body: Center(
        child: Column(
          children: <Widget>[
            Expended1(),
            Expanded(
              child: Container(color: Colors.yellow),
            ),
          ],
        ),
      ),
    );
  }
}

class Expended1 extends StatelessWidget {
  const Expended1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 4,
      child: Container(
        color: Colors.green,
        child: Column(
          children: <Widget>[
            Text("Omer"),
            Text("Yossi"),
          ],
        ),
      ),
    );
  }
}
